const mongoose = require('mongoose');

const NewsSchema = mongoose.Schema({
    name: String,
    newsDetails: String,
    posted_by:String,
    posted_on:{ type: Date, default: Date.now },
    user_id: Number
});

module.exports = mongoose.model('News', NewsSchema);