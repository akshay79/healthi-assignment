var cors = require('cors')
module.exports = (app) => {
    const news = require('../controllers/news.controller.js');

    // Create a new Note
    app.post('/news', cors(), news.create);

    // Retrieve all Notes
    app.get('/news',  cors(), news.findAll);

    // Delete a Note with noteId
    app.delete('/news/:newsId',  cors(), news.delete);
}