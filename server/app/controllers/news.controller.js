const News = require('../models/news.model.js');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if(!req.body.newsDetails) {
        return res.status(400).send({
            message: "News content can not be empty"
        });
    }

    // Create a Note
    const news = new News({
        name: req.body.name || "Untitled News", 
        newsDetails: req.body.newsDetails,
        posted_by:req.body.posted_by,
        posted_on : new Date(),
        user_id : req.body.user_id || 1
    });

    // Save Note in the database
    news.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    News.find()
    .then(notes => {
        res.send(notes);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};


// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    News.findByIdAndRemove(req.params.newsId)
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.newsId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.newsId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.newsId
        });
    });
};