const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors')
const dbConfig = require('./db.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose.connect(dbConfig.url)
.then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});

// create express app
const app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

app.options('*', cors())

// define a simple route
app.get('/', (req, res) => {
    res.json({"message": "Welcome"});
});


require('./app/routes/news.route.js')(app);

io.on('connection', function (socket) {
    socket.emit('news', { hello: 'world' });
    socket.on('my other event', function (data) {
      console.log(data);
    });
});

// listen for requests
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});