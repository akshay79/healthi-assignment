import React from 'react';
import { connect } from 'react-redux';

import {deleteNews, fetchNews} from './actions/index';
import { bindActionCreators } from 'redux';

import News from './News';
import NewPost from './NewPost';
import io from 'socket.io-client';

class Newsfeed extends React.Component{
	constructor(props){
		super(props);

		this.handleDelete = this.handleDelete.bind(this);
		this.handleFormShow = this.handleFormShow.bind(this);
	}

	state={
		hideDiv : true
	}

	componentDidMount(){
		this.props.fetchNews();
	}

	handleDelete(news){
		this.props.deleteNews(news);
	}

	handleFormShow(){
		this.setState({hideDiv:false});
	}
	handleCancel(){
		this.setState({hideDiv:true});
	}

	renderList(){
		return this.props.news.map((t)=>{
			return (
				<News 
					handleDelete={this.handleDelete} 
					news={t} 
					key={t._id}
				/>
			)
		})
	}
//value={this.state.taskName} 
	render(){
		let className = `row ${this.state.hideDiv ? 'hidden' : ''}`;
		return(
			<div className="containers">
				<div className="row">
					<div className="col-md-1"></div>
					<div className="col-md-10 news-container">
						<h1 className="my-4">Svasth Newsfeed
						<small></small>
						</h1><span className="pull-right"><button onClick={this.handleFormShow} className="btn btn-primary btn-lg my-4">Add News</button>	</span>
						<div className={className} style={{'marginBottom': '10px'}}>
							<div className="col-md-3"></div>
							<div className="col-md-6 form-new">
								<NewPost handleCancel={this.handleCancel.bind(this)}/>
							</div>
							<div className="col-md-3"></div>
						</div>
						<div className="row">
							<div className="col-md-1"></div>
							<div className="col-md-10 news-card">
								{this.renderList()}
							</div>
							<div className="col-md-1"></div>
						</div>
					</div>
					<div className="col-md-1"></div>
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => ({
  news: state.news
});

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		fetchNews,
		deleteNews
	},dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(Newsfeed);