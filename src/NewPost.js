import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';  
import {connect} from 'react-redux';
import {createNews} from './actions/index';

class NewPost extends Component {

    renderNameField(field){
        const {meta : {touched,error}} = field;
        const className = `form-group ${touched && error ? 'is-invalid' : ''}`;
        return(
            <div className={className}>
                <label>Title Name:</label>
                <input
                    className="form-control"
                    type="text"
                    {...field.input }
                />
                <div className="text-help">
                    {touched ? error : ''}
                </div>
            </div>
        )
    }

    renderDetailsField(field){
        const {meta : {touched,error}} = field;
        const className = `form-group ${touched && error ? 'is-invalid' : ''}`;
        return(
            <div className={className}>
                <label>Add Main News:</label>
                <textarea
                    className="form-control"
                    type="text"
                    {...field.input }
                />
                <div className="">
                    {touched ? error : ''}
                </div>
            </div>
        )
    }

    renderpostedField(field){
        const {meta : {touched,error}} = field;
        const className = `form-group ${touched && error ? 'is-invalid' : ''}`;
        return(
            <div className={className}>
                <label>Posted By:</label>
                <input
                    className="form-control"
                    type="text"
                    {...field.input }
                />
                <div className="">
                    {touched ? error : ''}
                </div>
            </div>
        )
    }

    onSubmit(values){
        //console.log(values);
        this.props.createNews(values);
        this.props.handleCancel();
    }

    handleCancel(){
        this.props.handleCancel();
    }

    render(){
        const {handleSubmit} = this.props;

        return(
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <Field
                    name="name"
                    label="Name"
                    component={this.renderNameField}
                />
                <Field
                    name="newsDetails"
                    label="Add Main news"
                    component={this.renderDetailsField}
                />
                <Field
                    name="posted_by"
                    label="Posted By"
                    component={this.renderpostedField}
                />
                <button style={{'marginBottom':'5px'}} type="submit" className="btn btn-primary">Submit</button>
                <button style={{'marginBottom':'5px'}} type="button" className="btn btn-danger" onClick={this.handleCancel.bind(this)}>Cancel</button>
            </form>
        )
    }
}

function validate(values){
    const errors = {};

    if(!values.name){
        errors.name = "Enter title name for news feed";
    }

    if(!values.newsDetails){
        errors.newsDetails = "Enter detailed News";
    }

    if(!values.posted_by){
        errors.posted_by = "Enter posted by";
    }

    return errors;
}

export default reduxForm({
    validate,
    form : 'newpost'
})(
    connect(null,{createNews})(NewPost)
);