import React from 'react';

const News = (props) => {
    return(
        <div>
            <div className="row news-header top-buffer">
                <div className="col-md-12">
                    <span><h5 className="news-title">{props.news.name}</h5></span>
                    <span className="trash-icon pull-right" onClick={()=>props.handleDelete(props.news)}>
                        <i className="fas fa-trash-alt"></i>
                    </span>
                </div>
            </div>

            <div className="row news-body top-buffer">
                <div className="col-md-1">
                    <div className="name-holder">AG</div>
                </div>
                <div className="col-md-10">
                    <span>{props.news.newsDetails}</span>
                </div>
            </div>

            <div className="row news-footer top-buffer">
                <div className="col-md-12">
                    <div><strong>Posted on: </strong>{props.news.posted_on}</div>
                    <div><strong>Posted by: </strong>{props.news.posted_by}</div>
                </div>
            </div>
        </div>
    )
}

export default News;