import React from 'react';
import ReactDOM from 'react-dom';
import Newsfeed from './Newsfeed';
import styles from './styles/main.scss';
import Navbar from './Navbar';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import rootReducer from "./reducers";

import ReduxPromise from 'redux-promise';

const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);

ReactDOM.render(
	 <Provider store={createStoreWithMiddleware(rootReducer)}>
		<div>
			<Navbar/>
			<div className="container">
				<Newsfeed/>
			</div>
		</div>
	</Provider>,
	document.getElementById("app")
);

module.hot.accept();