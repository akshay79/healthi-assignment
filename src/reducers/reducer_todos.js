export default function(state=[],action){
	switch(action.type){
        case 'NEWS_DELETED':
             const arr = state.filter(item=> {
                 return item._id!=action.payload[1]._id;
             })
			 return arr;
		case 'FETCH_NEWS':
			 return state.concat(action.payload.data);
		case 'CREATE_NEWS' : 
			 const newState = [...state];
			 newState.push(action.payload.data);
			 return newState;
    }
	return state;
}