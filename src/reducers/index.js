import { combineReducers } from 'redux';
import {reducer as formReducer} from 'redux-form'; 
import NewsReducer from "./reducer_todos";

const rootReducer = combineReducers({
  //state: (state = {}) => state
  news: NewsReducer,
  form : formReducer
});

export default rootReducer;