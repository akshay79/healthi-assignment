import axios from 'axios';
const ROOT_URL = 'http://localhost:3000/';

export function deleteNews (news){
    const url = `${ROOT_URL}news/${news._id}`;
    const req = axios.delete(url);
    return {
        type : 'NEWS_DELETED',
        payload: [req,news]
    };
}

export function fetchNews(){
    const url = `${ROOT_URL}news`;
    const req = axios.get(url);
    return {
        type : 'FETCH_NEWS',
        payload : req
    }
}

export function createNews(values){
    const url = `${ROOT_URL}news`;
    const req = axios.post(url,values);
    return {
        type : 'CREATE_NEWS',
        payload : req
    }
}