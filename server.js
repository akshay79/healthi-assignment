var express = require('express');
var app = express();
var port = process.env.port || 3001;
app.use(express.static(__dirname + '/dist'));

app.use('/dist',express.static(__dirname + '/dist'));
app.use('/api',express.static(__dirname + '/api'));
app.use('/assets',express.static(__dirname + '/assets'));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/dist/index.html');
});

app.listen(port);
console.log('Open this link http://localhost:'+port);